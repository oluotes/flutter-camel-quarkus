import 'package:admin/screens/dashboard/dashboard_screen.dart';
import 'package:admin/screens/login/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double paddingValue = 20.0;
    const double fontSize = 14.0;
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            //child: Image.asset("assets/images/teenme_logo_1x.png"),
            child: Container(
              // width: 200,
              // height: 200,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image:
                        Image.asset("assets/images/teenme_logo_1x.png").image,
                    fit: BoxFit
                        .contain), //Image.asset("assets/images/teenme_logo_1x.png"),
              ),
            ),
          ),
          DrawerListTile(
            title: "Dashboard",
            svgSrc: "assets/icons/menu_dashbord.svg",
            press: () {},
          ),
          ExpansionTile(
            leading: Icon(Icons.person),
            title: Text("Profile"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Profile Activation',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.receipt),
                  onTap: () => DashboardScreen(),
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Identity Verification',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'AML Query Status',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
            ],
          ),
          // Account Setup
          ExpansionTile(
            leading: Icon(Icons.settings_backup_restore),
            title: Text("Account"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Account Creation',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.receipt),
                  onTap: () => DashboardScreen(),
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Account Update',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Account Activity',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Account Activation',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Reward and Incentive',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
            ],
          ),
          //ransactipn
          ExpansionTile(
            leading: Icon(Icons.local_activity),
            title: Text("Transaction"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Dispute Resolution',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.receipt),
                  onTap: () => DashboardScreen(),
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Transaction Settlement',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Token Redemption',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
            ],
          ),
          // Advertesement
          ExpansionTile(
            leading: Icon(Icons.public),
            title: Text("Advertisement"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Subscription',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.receipt),
                  onTap: () => DashboardScreen(),
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Advert Configuration',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Metrics & Analytics',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
            ],
          ),
          ExpansionTile(
            leading: Icon(Icons.sell),
            title: Text("Merchant Setup"),
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Store creation',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.receipt),
                  onTap: () => DashboardScreen(),
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Inventory View',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.phone_in_talk),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: paddingValue),
                child: ListTile(
                  title: Text(
                    'Account Sweeping',
                    style: TextStyle(fontSize: fontSize),
                  ),
                  //leading: Icon(Icons.notification_important),
                  onLongPress: () {},
                  onTap: () => DashboardScreen(),
                ),
              ),
            ],
          ),
          // DrawerListTile(
          //   title: "Transaction",
          //   svgSrc: "assets/icons/menu_tran.svg",
          //   press: () {},
          // ),
          // DrawerListTile(
          //   title: "Task",
          //   svgSrc: "assets/icons/menu_task.svg",
          //   press: () {},
          // ),
          // DrawerListTile(
          //   title: "Documents",
          //   svgSrc: "assets/icons/menu_doc.svg",
          //   press: () {},
          // ),
          // DrawerListTile(
          //   title: "Store",
          //   svgSrc: "assets/icons/menu_store.svg",
          //   press: () {},
          // ),
          // DrawerListTile(
          //   title: "Notification",
          //   svgSrc: "assets/icons/menu_notification.svg",
          //   press: () {},
          // ),
          // DrawerListTile(
          //   title: "Profile",
          //   svgSrc: "assets/icons/menu_profile.svg",
          //   press: () {},
          // ),
          ListTile(
            title: Text('Settings'),
            leading: Icon(Icons.settings),
            onTap: () {},
          ),
          ListTile(
            title: Text('Logout'),
            leading: Icon(Icons.close),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return Login(
                      title: 'Login Screen',
                    );
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        color: Colors.white54,
        height: 16,
      ),
      title: Text(
        title,
        style: TextStyle(color: Colors.white54),
      ),
    );
  }
}
