apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: build-scan-push
spec:
  description: >-
    Buildah task builds source into a container image and then pushes it to a
    container registry.

    Buildah Task builds source into a container image using Project Atomic's
    Buildah build tool.It uses Buildah's support for building from Dockerfiles,
    using its buildah bud command.This command executes the directives in the
    Dockerfile to assemble a container image, then pushes that image to a
    container registry.
  
  params:
    - description: Reference of the image buildah will produce.
      name: IMAGE
      type: string
    - default: >-
        registry.redhat.io/rhel8/buildah@sha256:e19cf23d5f1e0608f5a897f0a50448beb9f8387031cca49c7487ec71bd91c4d3
      description: The location of the buildah builder image.
      name: BUILDER_IMAGE
      type: string
    - default: vfs
      description: Set buildah storage driver
      name: STORAGE_DRIVER
      type: string
    - default: ./Dockerfile
      description: Path to the Dockerfile to build.
      name: DOCKERFILE
      type: string
    - default: .
      description: Path to the directory to use as context.
      name: CONTEXT
      type: string
    - default: 'true'
      description: >-
        Verify the TLS on the registry endpoint (for push/pull to a non-TLS
        registry)
      name: TLSVERIFY
      type: string
    - default: oci
      description: 'The format of the built container, oci or docker'
      name: FORMAT
      type: string
    - default: ''
      description: Extra parameters passed for the build command when building images.
      name: BUILD_EXTRA_ARGS
      type: string
    - default: ''
      description: Extra parameters passed for the push command when pushing images.
      name: PUSH_EXTRA_ARGS
      type: string
    - name: SEVERITY_LEVELS
      description: Vulnerability severity level options are negligible, UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL
      default: UNKNOWN,LOW,MEDIUM,HIGH,CRITICAL
    - name: SCAN_TYPE
      description: Type of scan to perform (options are filesystem (fs), image (i), repository (repo))
      default: "filesystem"  
    - name: SCAN_PATH_OR_IMAGE_URL
      description: scan path or image url
      default: '/var/lib/containers'
    - name: IGNORE_UNFIXED
      default:  'false'
      description:  To ignore unpatched/unfixed vulnerabilities 
    - name: VULN_TYPE
      default:  'os,library'
      description:  Vulnerability type (options - os,library) Default if not specified - "os,library"
  results:
    - description: Digest of the image just built.
      name: IMAGE_DIGEST
  steps:
    - image: $(params.BUILDER_IMAGE)
      name: build
      resources: {}
      script: |
        buildah --storage-driver=$(params.STORAGE_DRIVER) bud \
          $(params.BUILD_EXTRA_ARGS) --format=$(params.FORMAT) \
          --tls-verify=$(params.TLSVERIFY) --no-cache \
          -f $(params.DOCKERFILE) -t $(params.IMAGE) $(params.CONTEXT)
      volumeMounts:
        - mountPath: /var/lib/containers
          name: varlibcontainers
      workingDir: $(workspaces.source.path)
    - name: scan-local-image
      image: registry.access.redhat.com/ubi8/ubi
      volumeMounts:
        - mountPath: /var/lib/containers
          name: varlibcontainers
      script: |
        #!/bin/bash
        set +x

        curl -sSfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin 

        echo "Trivy ..."
        IGNORE_UNFIXED=''
        if [ $(params.IGNORE_UNFIXED) == "true" ]; then  
            IGNORE_UNFIXED='--ignore-unfixed'
        fi
        VULN_TYPE=''
        if [ $(params.VULN_TYPE) != "" ]; then  
            VULN_TYPE='--vuln-type '$(params.VULN_TYPE)
        fi
        ls -la /workspace/local-image-repo 
        echo "Image to scan: "  $(params.SCAN_PATH_OR_IMAGE_URL)

        trivy $(params.SCAN_TYPE) --exit-code 1 --severity $(params.SEVERITY_LEVELS) \
        $IGNORE_UNFIXED $VULN_TYPE $(params.SCAN_PATH_OR_IMAGE_URL)

        retVal=$?
        echo "Return code is="$retVal
        if [ $retVal -ne 0 ]; then 
          echo "Scanning failed... vulnerability detected!"
        else
            echo "Scanning passed ... no vulnerability detected!" 
        fi
        exit $retVal
    - image: $(params.BUILDER_IMAGE)
      name: push
      resources: {}
      script: |
        buildah --storage-driver=$(params.STORAGE_DRIVER) push \
          $(params.PUSH_EXTRA_ARGS) --tls-verify=$(params.TLSVERIFY) \
          --digestfile $(workspaces.source.path)/image-digest $(params.IMAGE) \
          docker://$(params.IMAGE)
      volumeMounts:
        - mountPath: /var/lib/containers
          name: varlibcontainers
  volumes:
    - emptyDir: {}
      name: varlibcontainers
  workspaces:
    - name: source
  # volumes:  
  #   - name: varlibcontainers
  #     persistentVolumeClaim:
  #       claimName: image-repo-pvc
